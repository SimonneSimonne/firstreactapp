
import React, { useState } from 'react';
import './App.css';
import Quiz from './components/Quiz';

const App = () => {


    const [prenom, setPrenom] = useState('');
    const [isRempli, setisRempli] = useState(false);
    console.log(prenom);

    const handleInput = () => {
        setisRempli(true);
        console.log(isRempli)
    }

    const handlout = () => {
        setisRempli(false)
    }

    return (<div className="container">
        <div className="form">
            <h1 className="title">Bonjour</h1>
            <div>
                <input type="text" className="quiz" placeholder="Prénom" onChange={(e) => setPrenom(e.target.value)} value={prenom} />
                <button onClick={handleInput} className="btn">Ok</button >
            </div>

        </div>
        {isRempli ? <Quiz prenom={prenom} />  : null}
    </div>)

}

export default App;
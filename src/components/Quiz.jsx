import React from 'react';
import Component1 from './Component1/Component1';
import Component2 from './Component2/Component2';
import Component3 from './Component3/Component3';
import Component4 from './Component4/Component4';
import Component5 from './Component5/Component5';
import Component6 from './Component6/Component6';
import ComponentDefault from './ComponentDefault/ComponentDefault';
import '../components/Style.css'

const Quiz = ({ prenom }) => {

    const getComponent = (prenom) => {
        switch (prenom) {
            case 'Françoise':
                return <Component1 />;
            case 'Rémi':
                return <Component2 />;
            case 'Sylvain':
                return <Component3 />;
            case 'Eric':
                return <Component4 />;
            case 'Ricardo':
                return <Component5 />;
            case 'Laurent':
                return <Component5 />;
            default:
                return <ComponentDefault prenom={prenom} />;
        }

    }


    return (
        <div>
            {getComponent(prenom)}
        </div>
    )

}

export default Quiz;